import json
import math

a = []

for i in range(0,1000):
	val = i % 10 == 0;
	x = {
		"lat": 0.0 + math.sin(i) * (i / 100.0),
		"lon": 0.0 - math.cos(i) * (i / 100.0),
		"val": val
		}
	a.append(x)

with open("testdata.json","w") as f:
	contents = f.write(json.dumps(a))
