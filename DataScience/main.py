import urllib2
import json

import math
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt

def haversine(coord1, coord2):
    R = 6372800  # Earth radius in meters
    lat1, lon1 = coord1
    lat2, lon2 = coord2
    
    phi1, phi2 = math.radians(lat1), math.radians(lat2) 
    dphi       = math.radians(lat2 - lat1)
    dlambda    = math.radians(lon2 - lon1)
    
    a = math.sin(dphi/2)**2 + \
        math.cos(phi1)*math.cos(phi2)*math.sin(dlambda/2)**2
    
    return 2*R*math.atan2(math.sqrt(a), math.sqrt(1 - a))

#contents = urllib2.urlopen("<site>/values/GetAllSensorsData").read()
with open("testdata.json","r") as f:
	contents = f.read()
data = json.loads(contents);

bad_events = list(filter(lambda x: x["val"] == False, data));

x = []
y = []
z = []

for coord1 in data:
	val = 0.0
	for coord2 in data:
		dist = haversine((coord1["lat"],coord1["lon"]),(coord2["lat"],coord2["lon"]))
		if(dist != 0):
			val += 1/math.pow(dist,2)
		else: 
			val += 1
	x.append(coord1["lat"])
	y.append(coord1["lon"])
	z.append(val)
full_div = interpolate.interp2d(x, y, z, kind='cubic')

xb = []
yb = []
zb = []

for coord1 in bad_events:
	val = 0.0
	for coord2 in bad_events:
		dist = haversine((coord1["lat"],coord1["lon"]),(coord2["lat"],coord2["lon"]))
		if(dist != 0):
			val += 1/math.pow(dist,2)
		else: 
			val += 1
	xb.append(coord1["lat"])
	yb.append(coord1["lon"])
	zb.append(val)

bad_div = interpolate.interp2d(xb, yb, zb, kind='cubic')

zs = np.zeros((360,360), dtype = 'float32')

for xs in np.linspace(-180, 180, num=360):
	for ys in np.linspace(-180, 180, num=360):
		bad = bad_div(xs,ys)
		full = full_div(xs,ys)
		if full == 0:
			full = 1
		zs[xs,ys] =  bad;

maxz = np.amax(zs)
minz = np.amin(zs)

print maxz, minz

zs = zs - minz;
zs = zs / (maxz - minz);

maxz = np.amax(zs)
minz = np.amin(zs)

print maxz, minz

plt.plot(np.linspace(-180, 180, num=360), zs[:,0])

#plt.imshow(zs, interpolation='bilinear')
plt.show()
