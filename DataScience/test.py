import json
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import kde

with open("testdata.json","r") as f:
	contents = f.read()
data = json.loads(contents);

nbins = 20

data = np.array(map(lambda d: (d["lat"], d["lon"]), data))

fig, axes = plt.subplots(ncols=6, nrows=1, figsize=(21, 5))
x,y = data.T

print data

k = kde.gaussian_kde(data.T)
xi, yi = np.mgrid[x.min():x.max():nbins*1j, y.min():y.max():nbins*1j]
zi = k(np.vstack([xi.flatten(), yi.flatten()]))

axes[3].set_title('Calculate Gaussian KDE')
axes[3].pcolormesh(xi, yi, zi.reshape(xi.shape), cmap=plt.cm.BuGn_r)

plt.show()
